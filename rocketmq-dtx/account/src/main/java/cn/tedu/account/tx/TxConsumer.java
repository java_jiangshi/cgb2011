package cn.tedu.account.tx;

import cn.tedu.account.entity.AccountMessage;
import cn.tedu.account.service.AccountService;
import cn.tedu.account.util.JsonUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@RocketMQMessageListener(topic = "orderTopic",
                         consumerGroup = "account-consumer-group")
@Component
@Slf4j
public class TxConsumer implements RocketMQListener<String> {
    /*
    在消费者类中，调用业务对象，
    业务方法的执行，是靠消息来触发执行，

    现在执行业务方法，不是在controller中考客户端访问来实行
     */
    @Autowired
    private AccountService accountService;

    @Override
    public void onMessage(String json) {
        // json --> AccountMessage
        // 调用业务方法扣减金额
        AccountMessage am = JsonUtil.from(json, AccountMessage.class);
        accountService.decrease(am.getUserId(), am.getMoney());
        log.info("金额扣减成功");
    }
}
