package cn.tedu.order.service;

import cn.tedu.order.entity.AccountMessage;
import cn.tedu.order.entity.Order;
import cn.tedu.order.entity.TxInfo;
import cn.tedu.order.feign.AccountClient;
import cn.tedu.order.feign.EasyIdClient;
import cn.tedu.order.feign.StorageClient;
import cn.tedu.order.mapper.OrderMapper;
import cn.tedu.order.mapper.TxMapper;
import cn.tedu.order.util.JsonUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQTransactionListener;
import org.apache.rocketmq.spring.core.RocketMQLocalTransactionListener;
import org.apache.rocketmq.spring.core.RocketMQLocalTransactionState;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Random;
import java.util.UUID;

@RocketMQTransactionListener
@Service
@Slf4j
public class OrderServiceImpl
        implements OrderService, RocketMQLocalTransactionListener {
    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private EasyIdClient easyIdClient;
    @Autowired
    private RocketMQTemplate t;
    @Autowired
    private TxMapper txMapper;

    @Override
    public void create(Order order) {
        //产生一个 uuid 作为当前事务的事务id
        String xid = UUID.randomUUID().toString().replace("-", "");
        //用AccountMessage封装账户需要的业务数据，再转成 json
        String json = JsonUtil.to(new AccountMessage(
                order.getUserId(),
                order.getMoney(),
                xid
        ));
        //把 json 数据封装成 spring 的通用 message 对象
        Message<String> msg = MessageBuilder.withPayload(json).build();


        // 发送“半消息”，来触发业务代码执行
        // t.sendMessageInTransaction("orderTopic", msg, 本地业务需要处理的业务数据会被传递到监听器);
        t.sendMessageInTransaction("orderTopic", msg, order);
        log.info("已发送事务消息");
    }

    public void doCreate(Order order) {
        // 远程调用全局唯一ID发号器，生成订单id
        Long id = Long.valueOf(easyIdClient.nextId("order_business"));
        order.setId(id);
        orderMapper.create(order);
        if (Math.random() < 0.5) {
            throw new RuntimeException("模拟异常");
        }

        log.info("存储订单成功");
    }

    // 执行本地事务
    @Transactional
    @Override
    public RocketMQLocalTransactionState executeLocalTransaction(
            Message message, Object o) {
        RocketMQLocalTransactionState state; //用来返回
        int status; //用来向数据库表保存

        try {
            // 完成订单存储业务
            Order order = (Order) o;
            doCreate(order);
            state = RocketMQLocalTransactionState.COMMIT;
            status = 0;
        } catch (Exception e) {
            log.error("存储订单失败", e);
            state = RocketMQLocalTransactionState.ROLLBACK;
            status = 1;
        }

        // 存储本地事务执行状态
        // message {userId:1, money:10, xid:y4y34t34}
        String json = new String((byte[]) message.getPayload());
        String xid = JsonUtil.getString(json, "xid");
        TxInfo txInfo = new TxInfo(xid, status, System.currentTimeMillis());
        txMapper.insert(txInfo);

        log.info("模拟网络中断，无法提交或回滚事务消息");
        return RocketMQLocalTransactionState.UNKNOWN;
    }

    // 处理 rocketmq 服务器的事务回查
    @Override
    public RocketMQLocalTransactionState checkLocalTransaction(Message message) {
        log.info("服务器正在回查事务状态");

        String json = new String((byte[]) message.getPayload());
        String xid = JsonUtil.getString(json, "xid");
        TxInfo txInfo = txMapper.selectById(xid); //从数据库表查询事务状态
        if (txInfo == null) {
            return RocketMQLocalTransactionState.UNKNOWN; //没查询到状态数据，返回未知状态
        }
        switch (txInfo.getStatus()) {
            case 0: return RocketMQLocalTransactionState.COMMIT;
            case 1: return RocketMQLocalTransactionState.ROLLBACK;
            default: return RocketMQLocalTransactionState.UNKNOWN;
        }
    }
}
