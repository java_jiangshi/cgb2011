package cn.tedu.order.mapper;

import cn.tedu.order.entity.TxInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface TxMapper extends BaseMapper<TxInfo> {
    // 从 BaseMapper 继承的方法
    // insert()
    // selectById()
}
