package cn.tedu.order.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/*
下一个账户模块，需要处理的业务数据
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccountMessage {
    private Long userId;
    private BigDecimal money;
    private String xid;
}
