package cn.tedu.esspringboot.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;

@Document(indexName = "students")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Student {
    @Id //学生学号数据，作为索引id
    private Long id;
    private String name;
    private Character gender;
    private String birthDate;
}
