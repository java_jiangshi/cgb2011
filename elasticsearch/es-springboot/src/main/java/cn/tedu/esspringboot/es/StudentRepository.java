package cn.tedu.esspringboot.es;

import cn.tedu.esspringboot.entity.Student;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;

/*
Repository 接口规范，是 spring data 定义的一种数据操作规范，
不需要自己写代码，也无需添加注解配置

类型参数
    - Student: 访问的数据的类型
    - Long: id的数据类型

搜索方法:
    - 按照 ElasticsearchRepository 定义的方法命名规则来命名
    - findByNameAndPrice(): 按name和price，与的关系来搜索
    - findByNameOrPrice(): 按name和price，或的关系来搜索
    - findByName(): 按name搜索

    StudentRepository 可以理解成像 Mybatis 的 Mapper
 */
public interface StudentRepository extends ElasticsearchRepository<Student, Long> {
    List<Student> findByName(String name);

    Page<Student> findByNameOrBirthDate(
            String name, String birthDate, Pageable pageable);
}
