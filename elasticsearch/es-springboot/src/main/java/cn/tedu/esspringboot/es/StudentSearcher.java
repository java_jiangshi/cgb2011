package cn.tedu.esspringboot.es;

import cn.tedu.esspringboot.entity.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.query.Criteria;
import org.springframework.data.elasticsearch.core.query.CriteriaQuery;
import org.springframework.stereotype.Component;


import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class StudentSearcher {
    // 用来执行搜索请求的工具对象
    @Autowired
    private ElasticsearchOperations op;

    public List<Student> findByBirthDate(String birthDate) {
        // birthDate="xxxx-xx-xx"
        Criteria criteria = new Criteria("birthDate").is(birthDate);
        return query(criteria, null);
    }
    public List<Student> findByBirthDate(String from, String to, Pageable pageable) {
        Criteria criteria = new Criteria("birthDate").between(from, to);
        return query(criteria, pageable);
    }

    private List<Student> query(Criteria criteria, Pageable pageable) {
        //把 Criteria 搜索条件，封装成一个 Query 对象
        CriteriaQuery query = new CriteriaQuery(criteria);
        if (pageable != null) {
            query.setPageable(pageable);
        }

        SearchHits<Student> hits = op.search(query, Student.class);

        // // 用普通的集合操作遍历，生成一个新的List集合
        // List<Student> list = new ArrayList<>();
        // for (SearchHit<Student> hit : hits) {
        //     Student s = hit.getContent();
        //     list.add(s);
        // }

        // 用集合的“流”操作，来完成上面向通的功能
        List<Student> list =
                hits.stream().map(SearchHit::getContent).collect(Collectors.toList());

        return list;
    }
}
