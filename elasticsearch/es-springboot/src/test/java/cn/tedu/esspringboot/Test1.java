package cn.tedu.esspringboot;

import cn.tedu.esspringboot.entity.Student;
import cn.tedu.esspringboot.es.StudentRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.List;
import java.util.Optional;

@SpringBootTest
public class Test1 {
    @Autowired
    private StudentRepository r;

    @Test
    public void test1() {
        Student s1 = new Student(9527L, "唐伯虎", '男', "2021-04-14");
        Student s2 = new Student(9528L, "华夫人", '女', "2005-11-03");

        r.save(s1);
        System.out.println("----------------------------------------------------------");
        r.save(s2);
    }

    @Test
    public void test2() {
        Student s = new Student(9528L, "如花", '女', "2009-08-13");
        r.save(s);
    }

    @Test
    public void test3() {
        Student s = r.findById(9527L).get();
        System.out.println(s);
        System.out.println("-----------------------------------------------");
        Iterable<Student> it = r.findAll();
        for (Student s2: it) {
            System.out.println(s2);
        }
    }

    @Test
    public void test4() {
        List<Student> list = r.findByName("花");
        System.out.println(list);
    }

    @Test
    public void test5() {
        PageRequest p = PageRequest.of(0, 10);


        Page<Student> page = r.findByNameOrBirthDate("花", "2021-04-14", p);
        List<Student> list = page.getContent();
        System.out.println(page.getNumber());
        System.out.println(page.getTotalElements());
        System.out.println(page.getTotalPages());
        System.out.println(page.hasPrevious());
        System.out.println(page.hasNext());

        System.out.println(list);
    }
}
