package cn.tedu.esspringboot;

import cn.tedu.esspringboot.entity.Student;
import cn.tedu.esspringboot.es.StudentSearcher;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;

import java.util.List;

@SpringBootTest
public class Test2 {
    @Autowired
    private StudentSearcher searcher;

    @Test
    public void test1() {
        List<Student> list = searcher.findByBirthDate("2021-04-14");
        System.out.println(list);
    }

    @Test
    public void test2() {
        PageRequest p = PageRequest.of(0, 10);
        List<Student> list = searcher.findByBirthDate("2000-01-01", "2021-12-12", p);
        System.out.println(list);
    }
}
