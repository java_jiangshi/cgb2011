package m3;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.TimeoutException;

public class Producer {
    public static void main(String[] args) throws IOException, TimeoutException {
        // 1.连接
        ConnectionFactory f = new ConnectionFactory();
        f.setHost("wht6.cn");   // wht6.cn
        f.setPort(5672);
        f.setUsername("admin");
        f.setPassword("admin");
        Connection con = f.newConnection(); //连接
        Channel c = con.createChannel();    //通信通道

        // 创建交换机
        // 服务器端创建交换机，如果交换机已经存在不会重新创建
        // c.exchangeDeclare("logs", "fanout");
        c.exchangeDeclare("logs", BuiltinExchangeType.FANOUT);

        // 向交换机发送消息
        while (true) {
            System.out.print("输入消息： ");
            String s = new Scanner(System.in).nextLine();
            // 第二个参数不指定队列，即使指定也无效
            c.basicPublish("logs", "", null, s.getBytes());
        }
    }
}
