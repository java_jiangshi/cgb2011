package m1;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Producer {
    public static void main(String[] args) throws IOException, TimeoutException {
        // 1.连接
        ConnectionFactory f = new ConnectionFactory();
        f.setHost("192.168.64.140");   // wht6.cn
        f.setPort(5672);
        f.setUsername("admin");
        f.setPassword("admin");
        Connection con = f.newConnection(); //连接
        Channel c = con.createChannel();    //通信通道

        // 2.在服务器上创建 helloworld 队列
        // 服务器收到请求后会创建队列，如果队列已经存在，不会重新创建
        /*
        参数：
           1. 队列名
           2. 是否是持久队列
           3. 是否是排他队列（独占队列）
           4. 是否自动删除
           5. 其他属性设置
         */
        c.queueDeclare("helloworld",false,false,false,null);

        // 3.向 helloworld 队列发送消息
        /*
        参数：
           1. 默认的交换机
           3. 其他的消息属性设置
         */
        c.basicPublish("", "helloworld", null, "Hello world!".getBytes());
        System.out.println("消息已发送");

        // 断开
        c.close();
        con.close();
    }
}
