package m5;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.TimeoutException;

public class Producer {
    public static void main(String[] args) throws IOException, TimeoutException {
        // 1.连接
        ConnectionFactory f = new ConnectionFactory();
        f.setHost("192.168.64.140");   // wht6.cn
        f.setPort(5672);
        f.setUsername("admin");
        f.setPassword("admin");
        Connection con = f.newConnection(); //连接
        Channel c = con.createChannel();    //通信通道

        // 创建 Direct 交换机
        c.exchangeDeclare("topic_logs", BuiltinExchangeType.TOPIC);

        // 向交换机发送消息，消息上需要携带路由键关键词
        while (true) {
            System.out.println("输入消息： ");
            String s = new Scanner(System.in).nextLine();
            System.out.println("输入路由键： ");
            String k = new Scanner(System.in).nextLine();
            // 第二个参数： 路由键
            c.basicPublish("topic_logs", k, null, s.getBytes());
        }
    }
}
