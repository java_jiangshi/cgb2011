package cn.tedu.rabbitmqspringboot.m2;

import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Scanner;

@Component
public class Producer {
    @Autowired
    private AmqpTemplate t;

    public void send() {
        while (true) {
            System.out.println("输入消息：");
            String msg = new Scanner(System.in).nextLine();
            /*
            默认发送的是持久消息
             */
            t.convertAndSend("task_queue", msg);

            // // 发送非持久消息
            // // t.convertAndSend("task_queue", msg, 消息预处理对象);
            // t.convertAndSend("task_queue", (Object)msg, new MessagePostProcessor() {
            //     @Override
            //     public Message postProcessMessage(Message message) throws AmqpException {
            //         // 取出消息的属性对象
            //         MessageProperties p = message.getMessageProperties();
            //         // 设置成非持久消息
            //         p.setDeliveryMode(MessageDeliveryMode.NON_PERSISTENT);
            //         // 返回处理后的消息
            //         return message;
            //     }
            // });
        }

    }
}
