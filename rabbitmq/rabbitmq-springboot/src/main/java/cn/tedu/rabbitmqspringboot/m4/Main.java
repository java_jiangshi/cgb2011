package cn.tedu.rabbitmqspringboot.m4;

import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class Main {
    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }

    @Bean
    public DirectExchange exchange() {
        return new DirectExchange("direct_logs", false, false);
    }

    @Autowired
    private Producer p;

    /*
    spring主线程执行流程：
    扫描 --> 创建实例 --> 注入 --> @PostConstruct --> 继续执行后面步骤

    启动新的线程执行死循环发消息，避免阻塞spring主线程的执行
     */
    @PostConstruct
    public void test() {
        // new Thread(new Runnable() {
        //     @Override
        //     public void run() {
        //         p.send();
        //     }
        // }).start();

        // Lambda 表达式，对 Runnable 匿名内部类的语法简化
        new Thread(() -> p.send()).start();
    }
}
