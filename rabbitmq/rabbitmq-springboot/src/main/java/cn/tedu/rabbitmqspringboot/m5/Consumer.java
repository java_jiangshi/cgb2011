package cn.tedu.rabbitmqspringboot.m5;

import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class Consumer {

    //注册一个消费者
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue, //由rabbitmq服务器自动命名
            exchange = @Exchange(name = "topic_logs",declare = "false"), //在这里不定义交换机参数，在启动类中定义
            key = {"*.orange.*"}
    ))
    public void receive1(String msg) {
        System.out.println("消费者1收到： "+msg);
    }

    //注册第二个消费者
    @RabbitListener(bindings = @QueueBinding(
            // 使用 SPEL(Spring Expression Language)
            // 可以直接访问 spring 容器中的对象
            value = @Queue(name = "#{rndQueue.name}",declare = "false"), //由rabbitmq服务器自动命名
            exchange = @Exchange(name = "topic_logs",declare = "false"), //在这里不定义交换机参数，在启动类中定义
            key = {"*.*.rabbit", "lazy.#"}
    ))
    public void receive2(String msg) {
        System.out.println("消费者2收到： "+msg);
    }

}
