package cn.tedu.rabbitmqspringboot.m1;

import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class Main {
    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }

    //定义 helloworld 队列的参数
    //import org.springframework.amqp.core.Queue;
    @Bean
    public Queue helloworldQueue() {
        // return new Queue("helloworld"); // true,false,false --- 持久,非独占,不自动删除
        return new Queue("helloworld", false); //非持久
    }

    @Autowired
    private Producer p;

    /*
    在 spring 扫描完所有对象，创建了所有的实例，完成所有的依赖注入，
    然后会自动执行  @PostConstruct 注解的方法
     */
    @PostConstruct
    public void test() {
        p.send();
    }
}
