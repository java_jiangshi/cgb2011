package cn.tedu.rabbitmqspringboot.m3;

import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class Consumer {

    //注册一个消费者
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue, //由rabbitmq服务器自动命名
            exchange = @Exchange(name = "logs",declare = "false") //在这里不定义交换机参数，在启动类中定义
    ))
    public void receive1(String msg) {
        System.out.println("消费者1收到： "+msg);
    }

    //注册第二个消费者
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue, //由rabbitmq服务器自动命名
            exchange = @Exchange(name = "logs",declare = "false") //在这里不定义交换机参数，在启动类中定义
    ))
    public void receive2(String msg) {
        System.out.println("消费者2收到： "+msg);
    }

}
