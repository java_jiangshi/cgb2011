package cn.tedu.rabbitmqspringboot.m1;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Producer {
    @Autowired
    private AmqpTemplate t;
    /*
    自定义的 send() 方法，需要自己手动调用
     */
    public void send() {
        t.convertAndSend("helloworld", "Hello world!");
    }
}
