package cn.tedu.sp11.fallback;

import cn.tedu.web.util.JsonResult;
import com.sun.org.apache.bcel.internal.generic.RETURN;
import org.springframework.cloud.netflix.zuul.filters.route.FallbackProvider;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

@Component
public class ItemFB implements FallbackProvider {
    /*
    返回一个服务id，表示是针对这个服务的降级类
     */
    @Override
    public String getRoute() {
        return "item-service";
        // return "*"; //针对所有服务降级
        // return null;//针对所有服务降级
    }

    @Override
    public ClientHttpResponse fallbackResponse(String route, Throwable cause) {
        // 降级响应，封装在 Response 对象中
        return new ClientHttpResponse() {
            @Override
            public HttpStatus getStatusCode() throws IOException {
                return HttpStatus.OK;
            }
            @Override
            public int getRawStatusCode() throws IOException {
                return HttpStatus.OK.value();
            }
            @Override
            public String getStatusText() throws IOException {
                return HttpStatus.OK.getReasonPhrase();
            }
            @Override
            public void close() {
            }
            @Override
            public InputStream getBody() throws IOException {
                // jsonresult {code:200,msg:"转发调用商品服务失败",data:null}
                String json = JsonResult
                        .ok()
                        .msg("转发调用商品服务失败")
                        .toString();
                return new ByteArrayInputStream(json.getBytes("UTF-8"));
            }
            @Override
            public HttpHeaders getHeaders() {
                HttpHeaders h = new HttpHeaders();
                h.add("Content-Type", "application/json;charset=UTF-8");
                return h;
            }
        };
    }
}
