package cn.tedu.sp04.order.feign;

import cn.tedu.sp01.pojo.User;
import cn.tedu.web.util.JsonResult;
import org.springframework.stereotype.Component;

@Component
public class UserClientFB implements UserClient {
    @Override
    public JsonResult<User> getUser(Integer userId) {
        // 模拟有 redis 缓存
        if (Math.random() < 0.5) {
            return JsonResult.ok().data(new User(8, null, null));
        }

        return JsonResult.err().msg("调用用户服务失败");
    }
    @Override
    public JsonResult<?> addScore(Integer userId, Integer score) {
        return JsonResult.err().msg("调用用户服务失败");

    }
}
