package cn.tedu.sp04.order.feign;

import cn.tedu.sp01.pojo.Item;
import cn.tedu.web.util.JsonResult;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ItemClientFB implements ItemClient{
    @Override
    public JsonResult<List<Item>> getItems(String orderId) {
        // 模拟有 Redis 缓存数据
        if (Math.random() < 0.5) {
            // 50%概率返回模拟缓存数据
            List<Item> list = new ArrayList<>();
            list.add(new Item(1, "缓存商品1", 1));
            list.add(new Item(2, "缓存商品2", 4));
            list.add(new Item(3, "缓存商品3", 2));
            list.add(new Item(4, "缓存商品4", 6));
            list.add(new Item(5, "缓存商品5", 1));
            return JsonResult.ok().data(list);
        }

        return JsonResult.err().msg("调用商品服务失败");
    }
    @Override
    public JsonResult<?> decreaseNumber(List<Item> items) {
        return JsonResult.err().msg("调用商品服务失败");
    }
}
