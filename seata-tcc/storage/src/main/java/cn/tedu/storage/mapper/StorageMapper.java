package cn.tedu.storage.mapper;

import cn.tedu.storage.entity.Storage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface StorageMapper extends BaseMapper<Storage> {
    void decrease(Long productId, Integer count);

    // 根据id查询库存
    // 使用继承的 selectById()

    // 可用库存 --> 冻结
    void updateResidueToFrozen(Long productId, Integer count);

    // 冻结 --> 已售出
    void updateFrozenToUsed(Long productId, Integer count);

    // 冻结 --> 可用库存
    void updateFrozenToResidue(Long productId, Integer count);
}
