package cn.tedu.order.mapper;

import cn.tedu.order.entity.Order;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface OrderMapper extends BaseMapper<Order> {
    // Try 创建订单,冻结的订单
    void create(Order order);

    // Confirm 确认、提交，订单状态改成正常订单
    void updateStatus(Long orderId, Integer status);

    // Cancel 取消、回滚，删除订单
    // 使用继承的 deleteById()
}
