package cn.tedu.order.tcc;
/*
按照 seata 规范要求，添加 TCC 操作接口，
定义 TCC 三个操作方法
 */

import cn.tedu.order.entity.Order;
import io.seata.rm.tcc.api.BusinessActionContext;
import io.seata.rm.tcc.api.BusinessActionContextParameter;
import io.seata.rm.tcc.api.LocalTCC;
import io.seata.rm.tcc.api.TwoPhaseBusinessAction;

import java.math.BigDecimal;

@LocalTCC
public interface OrderTccAction {
    // 预留资源、冻结数据
    // BusinessActionContext 在两个阶段之间传递数据
    // BusinessActionContextParameter 把数据放入上下文对象
    // 为了避开 seata 的一个 bug， 这里不使用 order 封装对象
    // 而是分别接收 order 中数据
    @TwoPhaseBusinessAction(name = "OrderTccAction")
    boolean prepare(BusinessActionContext ctx,
                    @BusinessActionContextParameter(paramName = "id") Long id,
                    @BusinessActionContextParameter(paramName = "userId") Long userId,
                    @BusinessActionContextParameter(paramName = "productId") Long productId,
                    @BusinessActionContextParameter(paramName = "count") Integer count,
                    @BusinessActionContextParameter(paramName = "money") BigDecimal money);


    boolean commit(BusinessActionContext ctx);
    boolean rollback(BusinessActionContext ctx);
}
