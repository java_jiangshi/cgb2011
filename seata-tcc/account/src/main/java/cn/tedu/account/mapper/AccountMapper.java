package cn.tedu.account.mapper;

import cn.tedu.account.entity.Account;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.math.BigDecimal;

public interface AccountMapper extends BaseMapper<Account> {
    // 扣减账户金额
    void decrease(Long userId, BigDecimal money);

    // 根据用户id查询账户, 继承的方法 selectById()
    // 可用金额 --> 冻结
    void updateResidueToFrozen(Long userId, BigDecimal money);
    // 冻结 --> 已消费
    void updateFrozenToUsed(Long userId, BigDecimal money);
    // 冻结 --> 可用金额
    void updateFrozenToResidue(Long userId, BigDecimal money);
}
