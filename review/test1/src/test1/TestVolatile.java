package test1;

import java.util.Scanner;

public class TestVolatile {
    static volatile boolean b = true;

    public static void main(String[] args) throws InterruptedException {
        new Thread() {
            @Override
            public void run() {
                while (b) {
                    // 空操作
                }
                System.out.println("线程一： b被修改成了false");
            }
        }.start();

        Thread.sleep(1000); //让上面线程，先执行 1 秒


        new Thread() {
            @Override
            public void run() {
                System.out.println("线程二： 按回车，把 b 的值改成 false:");
                new Scanner(System.in).nextLine();
                b = false;
                System.out.println("线程二： b 的值已经被改成 false！");
            }
        }.start();
    }
}
