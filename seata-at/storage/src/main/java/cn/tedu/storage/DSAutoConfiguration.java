package cn.tedu.storage;

import com.zaxxer.hikari.HikariDataSource;
import io.seata.rm.datasource.DataSourceProxy;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;

@Configuration
public class DSAutoConfiguration {
    // 创建原始的数据源对象
    @Bean
    @ConfigurationProperties(prefix = "spring.datasource")
    public DataSource ds() {
        // druid、hikari
        return new HikariDataSource();
    }

    // 创建seata at事务的数据源代理对象
    @Bean
    @Primary  //首选对象
    public DataSource dsProxy(DataSource ds) {
        return new DataSourceProxy(ds);
    }
}
