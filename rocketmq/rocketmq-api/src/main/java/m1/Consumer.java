package m1;

import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.common.message.MessageExt;

import java.util.List;

public class Consumer {
    public static void main(String[] args) throws MQClientException {
        //创建消费者
        DefaultMQPushConsumer c = new DefaultMQPushConsumer("consumer-group1");
        //设置 name server 地址
        c.setNamesrvAddr("192.168.64.141:9876");

        //从 Topic1 订阅 "TagA || TagB || TagC" 消息
        c.subscribe("Topic1","TagA || TagB || TagC");
        //设置消息器听监，在器听监中处理消息，Concurrently器听监允许启动多个线程并行接收处理消息
        c.setMessageListener(new MessageListenerConcurrently() {
            @Override
            public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> msgs, ConsumeConcurrentlyContext context) {
                for (MessageExt ext : msgs) {
                    String s = new String(ext.getBody());
                    System.out.println("收到： "+s);
                }
                return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
                // return ConsumeConcurrentlyStatus.RECONSUME_LATER; //要求服务器稍后重新发送这条消息
            }
        });

        //启动消费者
        c.start();
    }
}
