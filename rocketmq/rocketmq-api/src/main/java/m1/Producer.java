package m1;

import org.apache.rocketmq.client.exception.MQBrokerException;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.exception.RemotingException;

import java.util.Scanner;

public class Producer {
    public static void main(String[] args) throws MQClientException, RemotingException, InterruptedException, MQBrokerException {
        // 创建生产者
        DefaultMQProducer p = new DefaultMQProducer("producer-group1");
        // 设置name server的地址
        p.setNamesrvAddr("192.168.64.141:9876");
        // 启动生产者（建立连接）
        p.start();

        while (true) {
            System.out.print("输入消息： ");
            String s = new Scanner(System.in).nextLine();
            // 创建消息对象
            // Topic相当于是一级分类, Tag相当于是二级分类
            Message msg = new Message("Topic1", "TagA", s.getBytes());
            if (Math.random()<0.5) {
                msg.setDelayTimeLevel(3);
            }




            // 发送消息
            SendResult r = p.send(msg);
            System.out.println(r);
        }
    }
}
