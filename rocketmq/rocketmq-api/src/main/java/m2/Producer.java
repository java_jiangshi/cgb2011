package m2;

import org.apache.rocketmq.client.exception.MQBrokerException;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.MessageQueueSelector;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.common.message.MessageQueue;
import org.apache.rocketmq.remoting.exception.RemotingException;

import java.util.List;

public class Producer {
    static String[] msgs = {
            "15103111039,创建",
                              "15103111065,创建",
            "15103111039,付款",
                                                 "15103117235,创建",
                              "15103111065,付款",
                                                 "15103117235,付款",
                              "15103111065,完成",
            "15103111039,推送",
                                                 "15103117235,完成",
            "15103111039,完成"
    };
    public static void main(String[] args) throws MQClientException, RemotingException, InterruptedException, MQBrokerException {
        //创建生产者
        DefaultMQProducer p = new DefaultMQProducer("producer-group2");
        //设置 name server
        p.setNamesrvAddr("192.168.64.141:9876");
        //启动
        p.start();

        //遍历发送消息，发消息时，设置队列选择器，使用订单id选择队列
        for (String msg : msgs) {
            // msg = "15103111039,创建"
            Long orderId = Long.valueOf(msg.split(",")[0]);
            Message m = new Message("Topic2", msg.getBytes());

            //依据指定数据，在所有队列中选择其中一个队列来发送消息
            //p.send(m, 队列选择器, 选择依据);
            SendResult r =
                p.send(m, new MessageQueueSelector() {
                    // 第三个参数是orderId
                    @Override
                    public MessageQueue select(List<MessageQueue> mqs, Message msg, Object arg) {
                        Long orderId = (Long) arg;
                        int index = (int) (orderId % mqs.size());
                        return mqs.get(index);
                    }
                }, orderId);

            System.out.println(r);

        }
    }
}
