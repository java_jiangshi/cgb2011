package m3;

import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.LocalTransactionState;
import org.apache.rocketmq.client.producer.TransactionListener;
import org.apache.rocketmq.client.producer.TransactionMQProducer;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.common.message.MessageExt;

import java.util.Scanner;

public class Producer {
    public static void main(String[] args) throws MQClientException {
        //创建事务消息生产者
        TransactionMQProducer p = new TransactionMQProducer("producer-group3");
        //设置 name server
        p.setNamesrvAddr("192.168.64.141:9876");
        //设置事务消息器听监
        //发送事务消息后，会触发这个器听监执行本地事务
        //还要在这里处理事务回查操作
        p.setTransactionListener(new TransactionListener() {
            // 执行本地事务的方法
            @Override
            public LocalTransactionState executeLocalTransaction(Message msg, Object arg) {
                if (Math.random() < 1) { // 100%
                    System.out.println("模拟由于网络中断，服务器无法获得事务状态");
                    return LocalTransactionState.UNKNOW;
                }

                System.out.println("执行本地事务");
                if (Math.random() < 0.5) {
                    System.out.println("本地事务执行成功");
                    return LocalTransactionState.COMMIT_MESSAGE;
                } else {
                    System.out.println("本地事务执行失败");
                    return LocalTransactionState.ROLLBACK_MESSAGE;
                }

                //return LocalTransactionState.COMMIT_MESSAGE; //向rocketmq发送提交指令，半消息变成可投递的消息
                //return LocalTransactionState.ROLLBACK_MESSAGE; //向rocketmq发送回滚指令，删除半消息
                //return LocalTransactionState.UNKNOW; //一般用不到，一般是由于网络中断，服务器无法获得事务状态
            }

            // 处理事务回查
            @Override
            public LocalTransactionState checkLocalTransaction(MessageExt msg) {
                System.out.println("服务器正在回查事务状态");
                if (Math.random() < 0.05) {
                    return LocalTransactionState.COMMIT_MESSAGE;
                }
                return LocalTransactionState.UNKNOW;
            }
        });
        //启动
        p.start();
        //发送事务消息
        while (true) {
            System.out.print("输入消息： ");
            String s = new Scanner(System.in).nextLine();
            Message msg = new Message("Topic3", s.getBytes());
            // p.sendMessageInTransaction(msg, 执行本地事务需要的业务数据);
            p.sendMessageInTransaction(msg, null);
        }
    }
}

