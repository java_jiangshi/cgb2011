package com.pd;

import com.pd.pojo.PdOrder;
import com.pd.service.OrderService;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

// 通过注解配置，从 orderQueue 队列接收消息
@RabbitListener(queues = "orderQueue")
@Component
public class OrderConsumer {
    @Autowired
    private OrderService orderService;

    // 配合 RabbitListener 注解，指定处理消息的方法
    @RabbitHandler
    public void receive(PdOrder order) throws Exception {
        System.out.println("--收到订单消息，开始存储订单-------------------------------------");
        orderService.saveOrder(order);
    }
}
