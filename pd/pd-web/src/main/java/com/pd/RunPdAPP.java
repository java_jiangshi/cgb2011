package com.pd;


import org.mybatis.spring.annotation.MapperScan;
import org.springframework.amqp.core.Queue;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@MapperScan("com.pd.mapper")
public class RunPdAPP{
	
	public static void main(String[] args) {
		SpringApplication.run(RunPdAPP.class, args);
	}

	// 在启动类中，或者单独的自动配置类中创建都可以
	// 创建 Queue 实例，来封装队列的参数
	// Rabbitmq自动配置类中，会自动发现这个Queue实例，
	// 并根据封装的参数，在rabbitmq服务器上创建队列

	// import org.springframework.amqp.core.Queue;
	@Bean
	public Queue orderQueue() {
		return new Queue("orderQueue",false,false,false);
	}
}
