package com.pd.controller;

import com.pd.pojo.Item;
import com.pd.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class SearchController {
    @Autowired
    private SearchService service;

    /*
    不用自己创建 pageable 对象，只需要添加参数
     */
    @GetMapping("/search/toSearch.html")
    public String search(String key, Pageable pageable, Model model) {
        pageable.getPageSize()
        Page<Item> page = service.search(key, pageable);
        model.addAttribute("page", page);
        return "/search.jsp";
    }
}
